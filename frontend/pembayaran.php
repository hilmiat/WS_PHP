<?php

$curl = curl_init();
$mr = $_GET['mr'];
curl_setopt_array($curl, array(
  CURLOPT_PORT => "81",
  CURLOPT_URL => "http://localhost:81/PHPWS/demo.php/pasien/$mr",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "PUT",
  CURLOPT_POSTFIELDS => "{\"status\":2}",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: a221fca0-a879-6b27-f122-e1fd6d97e204"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  header('location:http://localhost:81/PHPWS/frontend/pasien2.php');
  echo $response;
}


 ?>
