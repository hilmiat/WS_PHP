<?php
class ModelPasien{
  $db;
  function __construct(){
    $this->db = new PDO("mysql:host=$dbhost;dbname=$dbname",
              $dbuser,$dbpass);
  }
  function insertData($input){
    $st = $this->db->prepare("INSERT INTO pasien
      (mr,no_pend,nama,tanggal,status)
      values(?,?,?,now(),?)");
    //eksekusi statement
    $data=array();
    $data[] =  $input->mr;
    $data[] =  $input->no_pend;
    $data[] =  $input->nama;
    $data[] =  $input->status;
    $status = $st->execute($data);
    return $status;
  }
}

?>
