<?php


  //baca method
  $method = $_SERVER['REQUEST_METHOD'];
  //echo $method;
  //baca path
  $path = $_SERVER['PATH_INFO'];
  //echo $path;
  $path_params =
    preg_split("/\/+/",$path);
    //print_r($path_params);

switch ($method){
  case 'GET'://---get all pasien
    if($path_params[1]!=null &&
    $path_params[1]=='pasien'){
      if(isset($_GET['nama'])){
          $data = getByNama($_GET['nama']);

      }else{
        $data = getAllPasien();
      }
      echo json_encode($data);
    }
    break;
  case 'POST':
    if($path_params[1]!=null &&
    $path_params[1]=='pasien'){
      $input = file_get_contents("php://input");
      $status = insertData(json_decode($input));
      if($status){
        echo 'sukses insert';
      }else{
        echo 'gagal insert';
      }
    }
    break;
  case 'PUT':
      if($path_params[1]!=null &&
      $path_params[1]=='pasien'){
        $input = file_get_contents("php://input");
        $mr = $path_params[2];
        $status = updateData($mr,json_decode($input));
        if($status){
          echo 'sukses update';
        }else{
          echo 'gagal update';
        }
      }
      break;
    case 'DELETE':
          if($path_params[1]!=null &&
          $path_params[1]=='pasien'){
            $mr = $path_params[2];
            $status = deleteData($mr);
            if($status){
              echo 'sukses delete';
            }else{
              echo 'gagal delete';
            }
          }
      break;
  default:
    echo 'method not implemented';
}

function getAllPasien(){
  //sertakan file koneksi
  require('koneksi.php');
  //create statement
  $st = $db->prepare("SELECT * FROM pasien");
  //eksekusi statement
  $st->execute();
  //ambil seluruh baris
  $result = $st->fetchAll();
  return $result;
}

function getByNama($nama){
  //sertakan file koneksi
  require('koneksi.php');
  //create statement
  $st = $db->prepare("SELECT * FROM pasien WHERE nama like ?");
  //eksekusi statement
  $st->execute(array($nama));
  //ambil seluruh baris
  $result = $st->fetchAll();
  return $result;
}

function insertData($input){
  //sertakan file koneksi
  require('koneksi.php');
  //create statement
  $st = $db->prepare("INSERT INTO pasien
    (mr,no_pend,nama,tanggal,status)
    values(?,?,?,now(),?)");
  //eksekusi statement
  $data=array();
  $data[] =  $input->mr;
  $data[] =  $input->no_pend;
  $data[] =  $input->nama;
  $data[] =  $input->status;
  $status = $st->execute($data);
  return $status;
}
function updateData($mr,$input){
  //sertakan file koneksi
  require('koneksi.php');
  //create statement
  $st = $db->prepare("UPDATE pasien
                      SET status=? where mr=?");
  //eksekusi statement
  $data=array();
  $data[] =  $input->status;
  $data[] =  $mr;
  $status = $st->execute($data);
  return $status;
}
function deleteData($mr){
  //sertakan file koneksi
  require('koneksi.php');
  //create statement
  $st = $db->prepare("DELETE FROM pasien
                     where mr=?");
  //eksekusi statement
  $status = $st->execute(array($mr));
  return $status;
}
?>
